from datetime import datetime
from datetime import timedelta

greeting = "Hello, GitHub! I`m here :-)"
current_date = datetime.now().date()
current_time = datetime.now().time()
tomorrow = datetime.now().date() + timedelta(days=1)
print(greeting)
print("Today is:", current_date)
print("Now:", current_time)
# print("Tomorrow:", tomorrow)


